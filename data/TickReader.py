import requests
import json
import pandas as pd
import io

_ticker_base_url_ = "https://api-pub.bitfinex.com/v2"
_historical_base_url_ = "http://www.cryptodatadownload.com/cdd/Gemini_{}_d.csv"


def get_all_tickers():
    all_ticker_params = {'symbols': 'ALL'}
    response = requests.get(_ticker_base_url_ + "/tickers", params=all_ticker_params)
    return json.loads(response.text)


def get_ticker(symbol):
    response = requests.get(_ticker_base_url_ + "/ticker/t" + symbol)
    return tick_array_to_dict(json.loads(response.text))


def tick_array_to_dict(tick_array):
    return {"bid": tick_array[0],
            "bid_size": tick_array[1],
            "ask": tick_array[2],
            "ask_size": tick_array[3],
            "daily_change": tick_array[4],
            "daily_change_perc": tick_array[5],
            "last_price": tick_array[6],
            "volume": tick_array[7],
            "high": tick_array[8],
            "low": tick_array[9]
            }


def get_historical_data(symbol):
    url = _historical_base_url_.format(symbol)
    response = requests.get(url)

    response_text = response.text
    sansfirstline = '\n'.join(response_text.split('\n')[1:])
    df = pd.read_csv(io.StringIO(sansfirstline))
    return df


# print(get_historical_data('BTCUSD'))
