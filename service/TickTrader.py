from data.TickReader import get_ticker


def trade(symbol, perc_movement):
    if perc_movement > 0:
        trade_buy(symbol)
    elif perc_movement < 0:
        trade_sell(symbol)


def trade_buy(symbol):
    buy_price = get_buy_price(symbol)
    print('Buying {} at price {}'.format(symbol, buy_price))


def trade_sell(symbol):
    sell_price = get_sell_price(symbol)
    print('Selling {} at price {}'.format(symbol, sell_price))


def get_sell_price(symbol):
    ticker_data = get_ticker(symbol)
    return ticker_data['bid']


def get_buy_price(symbol):
    ticker_data = get_ticker(symbol)
    return ticker_data['ask']


# trade('BTCUSD', 0.049)
# trade('LTCUSD', -0.049)
