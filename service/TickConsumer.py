import matplotlib.pyplot as plt

from data.TickReader import get_all_tickers, get_historical_data, get_ticker
from service.TickTrader import trade

_crypto_names_list_ = []
for tick in get_all_tickers():
    _crypto_names_list_.append((tick[0])[1:])


def get_data_frame(crypto_name):
    df = get_historical_data(crypto_name)
    df = df[['Close']]
    df.iloc[:] = df.iloc[::-1].values
    df.reset_index(level=0, inplace=True)
    df.columns = ['x', 'y']
    return df


def plot_sma(crypto_name):
    print("Plotting SMA graph for " + crypto_name)

    df = get_data_frame(crypto_name)
    short_term_mean = df.y.rolling(window=20).mean()
    long_term_mean = df.y.rolling(window=50).mean()

    # Simple moving average graph

    plt.plot(df.x, df.y, label=crypto_name)
    plt.plot(df.x, short_term_mean, label=crypto_name + ' 20 Day SMA', color='orange')
    plt.plot(df.x, long_term_mean, label=crypto_name + ' 50 Day SMA', color='magenta')
    plt.legend(loc='upper left')
    plt.show()


def plot_ema(crypto_name):
    print("Plotting EMA graph for " + crypto_name)

    df = get_data_frame(crypto_name)
    exp1 = df.y.ewm(span=20, adjust=False).mean()
    exp2 = df.y.ewm(span=50, adjust=False).mean()

    # Exponential moving average graph

    plt.plot(df.x, df.y, label=crypto_name)
    plt.plot(df.x, exp1, label=crypto_name + '20 Day EMA')
    plt.plot(df.x, exp2, label=crypto_name + '50 Day EMA')
    plt.legend(loc='upper left')
    plt.show()


def get_percentage_change(short, long):
    return (short - long) / short


def check_if_should_trade(crypto_name):
    df = get_data_frame(crypto_name)
    short_term_mean = df.y.rolling(window=20).mean()
    long_term_mean = df.y.rolling(window=50).mean()
    trade(crypto_name, get_percentage_change(short_term_mean[short_term_mean.keys()[-1]],
                                             long_term_mean[long_term_mean.keys()[-1]]))
    # print("Ticker data: ")
    # print(get_ticker(crypto_name))
    # plot_sma(crypto_name)


def do_trades(crypto_names_list):
    for crypto_name in crypto_names_list:
        check_if_should_trade(crypto_name)


def do_trade(crypto_name):
    check_if_should_trade(crypto_name)


do_trade(_crypto_names_list_[0])
do_trade(_crypto_names_list_[1])
do_trade(_crypto_names_list_[3])
